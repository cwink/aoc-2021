#include "../include/aoc.hpp"

#include <algorithm>
#include <bitset>
#include <iterator>
#include <numeric>
#include <type_traits>
#include <vector>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int // NOLINT(readability-function-cognitive-complexity)
{
  return aoc::run([]() { // NOLINT(readability-function-cognitive-complexity)
    constexpr auto width{5U};

    constexpr auto height{5U};

    constexpr auto dimensions{width * height};

    using Board = std::array<std::int16_t, dimensions>;

    const auto numbers{[buffer{std::vector<std::uint16_t>{}}]() mutable {
      for (auto it{std::istream_iterator<std::uint16_t>(std::cin)};; it = std::istream_iterator<std::uint16_t>(std::cin)) {
        buffer.emplace_back(*it);

        auto nit{std::find_if(std::istreambuf_iterator<char>(std::cin), std::istreambuf_iterator<char>(),
                              [](const auto ch) { return aoc::is_digit(ch) || ch == '\n'; })};

        if (*nit == '\n') {
          break;
        }
      }

      return buffer;
    }()};

    auto boards{[buffer{std::vector<Board>{}}]() mutable {
      for (auto it{std::istream_iterator<std::uint16_t>(std::cin)}, eit{std::istream_iterator<std::uint16_t>()}; it != eit;
           it = std::istream_iterator<std::uint16_t>(std::cin)) {
        auto board{Board{}};

        std::copy_n(it, dimensions, std::begin(board));

        buffer.emplace_back(board);

        std::find_if(std::istreambuf_iterator<char>(std::cin), std::istreambuf_iterator<char>(), aoc::is_digit); // NOLINT(bugprone-unused-return-value)
      }

      return buffer;
    }()};

    for (const auto number : numbers) {
      for (auto &board : boards) {
        auto *it{std::find(std::begin(board), std::end(board), number)};

        if (it != std::end(board)) {
          *it = -1;
        }
      }

      auto bit{std::begin(boards)};

      auto beit{std::end(boards)};

      const auto value_at = [&bit](const std::uint32_t x, const std::uint32_t y) {
        return (*bit)[(y * width) + x]; // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
      };

      const auto process_board = [&value_at]() {
        for (auto index{0U}; index < width; ++index) {
          if ((value_at(index, 0) == -1 && value_at(index, 1) == -1 && value_at(index, 2) == -1 && value_at(index, 3) == -1 && value_at(index, 4) == -1) ||
              (value_at(0, index) == -1 && value_at(1, index) == -1 && value_at(2, index) == -1 && value_at(3, index) == -1 && value_at(4, index) == -1)) {
            return false;
          }
        }

        return true;
      };

      for (; bit != beit && process_board(); ++bit) {
      }

      if (bit != beit) {
        return std::accumulate(std::cbegin(*bit), std::cend(*bit), 0, [](const auto result, const auto val) { return val == -1 ? result : (result + val); }) *
               number;
      }
    }

    return -1;
  });
}
