#include "../include/aoc.hpp"

#include <algorithm>
#include <iterator>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int
{
  return aoc::run([]() {
    auto horizontal{0U};

    auto depth{0L};

    auto aim{0L};

    for (auto it{std::istreambuf_iterator<char>(std::cin)}, eit{std::istreambuf_iterator<char>()}; it != eit; it = std::istreambuf_iterator<char>(std::cin)) {
      const auto command{*it};

      it = std::find_if(it, eit, aoc::is_space);

      it = std::find_if_not(it, eit, aoc::is_space);

      const auto value{*std::istream_iterator<std::uint32_t>(std::cin)};

      it = std::find_if_not(it, eit, aoc::is_space);

      switch (command) {
      case 'f':
        horizontal += value;

        depth += aim * value;

        break;
      case 'd':
        aim += value;

        break;
      case 'u':
        aim -= value;

        break;
      default:
        break;
      }
    }

    return horizontal * depth;
  });
}
