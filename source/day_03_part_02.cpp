#include "../include/aoc.hpp"

#include <algorithm>
#include <bitset>
#include <iterator>
#include <type_traits>
#include <vector>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int
{
  return aoc::run([]() {
    using Bits = std::bitset<32>; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

    auto bits{std::vector<Bits>{}};

    bits.reserve(1024); // NOLINT(cppcoreguidelines-avoid-magic-numbers)

    const auto bit_length{aoc::count_until_if(std::istreambuf_iterator<char>(std::cin), std::istreambuf_iterator<char>(), aoc::is_space)};

    std::cin.seekg(0);

    for (auto it{std::istreambuf_iterator<char>(std::cin)}, eit{std::istreambuf_iterator<char>()}; it != eit;) {
      auto bit{Bits{}};

      auto index{bit_length - 1};

      it = aoc::for_each_until_if(
          it, eit, [&index, &bit](const auto ch) { bit.set(index--, ch - '0'); }, aoc::is_space);

      it = std::find_if_not(it, eit, aoc::is_space);

      bits.emplace_back(bit);
    }

    const auto get_rating = [&bits, bit_length](const auto predicate, const auto last_value) {
      auto ratings{bits};

      for (auto index{bit_length}; index - 1 > 0; --index) {
        auto zeros{0U};

        auto ones{0U};

        if (ratings.size() == 1) {
          break;
        }

        for (const auto &bit : ratings) {
          zeros += static_cast<std::uint8_t>(!bit[index - 1]);

          ones += static_cast<std::uint8_t>(bit[index - 1]);
        }

        auto rit{std::remove_if(std::begin(ratings), std::end(ratings),
                                [zeros, ones, index, &predicate](const auto &bit) { return bit[index - 1] == predicate(zeros, ones); })};

        ratings.erase(rit, std::end(ratings));
      }

      if (bits.size() != 1) {
        auto rit{std::remove_if(std::begin(ratings), std::end(ratings), [last_value](const auto &bit) { return bit[0] != last_value; })};

        ratings.erase(rit, std::end(ratings));
      }

      return ratings;
    };

    const auto oxygen{get_rating([](const auto zeros, const auto ones) { return ones < zeros; }, 1)};

    const auto scrubber{get_rating([](const auto zeros, const auto ones) { return zeros <= ones; }, 0)};

    return oxygen[0].to_ulong() * scrubber[0].to_ulong();
  });
}
