#include "../include/aoc.hpp"

#include <algorithm>
#include <iterator>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int
{
  return aoc::run([]() {
    auto previous_values{std::array<std::uint32_t, 4>{0, 0, 0, 0}};

    auto it{std::istream_iterator<std::uint32_t>(std::cin)};

    previous_values[0] = *it;

    previous_values[1] = *std::next(it);

    previous_values[2] = *std::next(it);

    previous_values[3] = *std::next(it);

    const auto calculate{[&previous_values](const std::uint32_t value) -> bool {
      const auto result{(previous_values[1] + previous_values[2] + previous_values[3]) > (previous_values[0] + previous_values[1] + previous_values[2])};

      previous_values[0] = previous_values[1];

      previous_values[1] = previous_values[2];

      previous_values[2] = previous_values[3];

      previous_values[3] = value;

      return result;
    }};

    return std::count_if(std::istream_iterator<std::uint32_t>(std::cin), std::istream_iterator<std::uint32_t>(), calculate) + (calculate(0) ? 1 : 0);
  });
}
