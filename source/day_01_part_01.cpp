#include "../include/aoc.hpp"

#include <algorithm>
#include <iterator>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int
{
  return aoc::run([]() {
    auto previous_value{0U};

    return std::count_if(std::istream_iterator<std::uint32_t>(std::cin), std::istream_iterator<std::uint32_t>(),
                         [&previous_value](const auto value) {
                           const auto result{value > previous_value};

                           previous_value = value;

                           return result;
                         }) -
           1;
  });
}
