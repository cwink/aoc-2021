#include "../include/aoc.hpp"

#include <algorithm>
#include <bitset>
#include <iterator>
#include <type_traits>
#include <vector>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) -> int
{
  return aoc::run([]() {
    auto buffer{std::vector<char>{}};

    auto zeros{std::vector<std::uint32_t>{}};

    auto it{std::istreambuf_iterator<char>(std::cin)};

    auto eit{std::istreambuf_iterator<char>()};

    const auto bit_length{aoc::count_until_if(it, eit, aoc::is_space)};

    buffer.reserve(bit_length);

    std::fill_n(std::back_inserter(zeros), bit_length, 0);

    std::cin.seekg(0);

    auto entry_count{0ULL};

    for (; it != eit;) {
      auto zit{std::begin(zeros)};

      it = aoc::for_each_until_if(
          it, eit, [&zit](const auto ch) { *zit++ += static_cast<std::uint32_t>(ch - '0'); }, aoc::is_space);

      it = std::find_if_not(it, eit, aoc::is_space);

      ++entry_count;

      buffer.clear();
    }

    auto index{0ULL};

    auto gamma{std::bitset<32>{}}; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

    auto epsilon{std::bitset<32>{}}; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

    std::for_each(std::crbegin(zeros), std::crend(zeros), [&index, &gamma, &epsilon, entry_count](const auto value) {
      gamma.set(index, value < (entry_count - value));

      epsilon.set(index, value > (entry_count - value));

      ++index;
    });

    return gamma.to_ulong() * epsilon.to_ulong();
  });
}
