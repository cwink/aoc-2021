CC := $(or $(CC),$(CC),clang++)

CPP_STANDARD := $(or $(CPP_STANDARD),$(CPP_STANDARD),c++17)

DEBUG := $(or $(DEBUG),$(DEBUG),true)

CPPFLAGS := $(CPPFLAGS) -std=$(CPP_STANDARD)

SOURCE_DIR := source

BUILD_DIR := build

DATA_DIR := data

ifeq ($(CC),clang++)
	CPPFLAGS_COMPILER := $(CPPFLAGS_COMPILER) -Weverything
else
	CPPFLAGS_COMPILER := $(CPPFLAGS_COMPILER) -Wall -Wextra
endif

ifeq ($(shell echo '${DEBUG}' | tr '[:upper:]' '[:lower:]'),true)
	CPPFLAGS := $(CPPFLAGS) $(CPPFLAGS_COMPILER) -Wno-c++98-compat -Wno-c++98-c++11-compat-binary-literal -Wno-c++98-compat-pedantic -pedantic-errors -g -O0
else
	CPPFLAGS := $(CPPFLAGS) -O2
endif

SOURCE_FILES := $(wildcard $(SOURCE_DIR)/*.cpp)

BIN_FILES := $(patsubst $(SOURCE_DIR)/%.cpp,$(BUILD_DIR)/%.x,$(SOURCE_FILES))

DAT_FILES := $(patsubst $(SOURCE_DIR)/%.cpp,$(DATA_DIR)/%.dat,$(SOURCE_FILES))

.PHONY: clean run

all: $(BIN_FILES)

$(BUILD_DIR)/%.x : $(SOURCE_DIR)/%.cpp
	$(CC) $(CPPFLAGS) -o $@ $<

$(DATA_DIR)/%.dat : $(BUILD_DIR)/%.x
	$< <$@

clean:
	rm -rf $(BUILD_DIR)/*.x

run: $(DAT_FILES)

