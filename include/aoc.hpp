#ifndef AOC_HPP
#define AOC_HPP

#include <array>
#include <chrono>
#include <cstddef>
#include <iostream>
#include <stdexcept>

extern "C" {
#include <sys/select.h>
#include <unistd.h>
}

using timeval = struct timeval;

namespace aoc {
inline auto is_stdin_valid() noexcept -> bool
{
  auto read_fds{fd_set{}};

  auto timeout{timeval{0, 0}};

  FD_ZERO(&read_fds); // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)

  FD_SET(STDIN_FILENO, &read_fds); // NOLINT(hicpp-signed-bitwise)

  return select(1, &read_fds, nullptr, nullptr, &timeout) > 0;
}

namespace details {
constexpr auto padding_size(const std::size_t size) -> std::size_t
{
  auto max = alignof(std::max_align_t);

  for (; size > max; max += alignof(std::max_align_t)) {
  }

  return max - size;
}
} // namespace details

template<typename... Types>
using Padding = std::array<std::uint8_t, details::padding_size((alignof(Types) + ...))>;

namespace details {
template<typename Type>
using has_now = std::enable_if_t<std::is_same_v<std::void_t<decltype(std::declval<Type>().now())>, void>>;

template<typename Type, typename = has_now<Type>>
class Clock final {
  using Now = decltype(std::declval<Type>().now());

public:
  Clock() = default;

  Clock(const Clock &) = delete;

  Clock(Clock &&) noexcept = default;

  auto operator=(const Clock &) -> Clock & = delete;

  auto operator=(Clock &&) noexcept -> Clock & = default;

  ~Clock() noexcept = default;

  constexpr auto start() -> void
  {
    this->start_ = Type::now();

    this->running_ = true;
  }

  constexpr auto stop() -> void
  {
    this->stop_ = Type::now();

    this->running_ = false;
  }

  constexpr auto seconds() -> double
  {
    if (this->running_) {
      throw std::runtime_error("clock is still running");
    }

    return std::chrono::duration<double>{this->stop_ - this->start_}.count();
  }

private:
  Now start_{};

  Now stop_{};

  bool running_{false};

  const Padding<Now, Now, bool> padding_{};
};
} // namespace details

using Clock = details::Clock<std::chrono::high_resolution_clock>;

template<typename Runner>
inline auto run(const Runner &runner) -> int
{
  if (!aoc::is_stdin_valid()) {
    std::cerr << "Please provide input." << '\n';

    return EXIT_FAILURE;
  }

  auto clock{aoc::Clock{}};

  clock.start();

  try {
    std::cout << "Result: " << runner() << '\n' << '\n';
  } catch (const std::exception &ex) {
    std::cerr << "ERROR: " << ex.what() << '\n';

    return EXIT_FAILURE;
  }

  clock.stop();

  std::cout << "Time: " << clock.seconds() << " seconds." << '\n';

  return EXIT_SUCCESS;
}

inline auto is_space(const char ch) -> bool
{
  return std::isspace(static_cast<unsigned char>(ch)) != 0;
}

inline auto is_not_space(const char ch) -> bool
{
  return !is_space(ch);
}

inline auto is_digit(const char ch) -> bool
{
  return std::isdigit(static_cast<unsigned char>(ch)) != 0;
}

inline auto is_not_digit(const char ch) -> bool
{
  return !is_digit(ch);
}

template<typename InputIterator, typename OutputIterator, typename Type = decltype(*std::declval<InputIterator>())>
inline auto copy_until(InputIterator begin, InputIterator end, OutputIterator dest, const Type &value) -> InputIterator
{
  for (; begin != end && *begin != value; ++begin, ++dest) {
    *dest = *begin;
  }

  return begin;
}

template<typename InputIterator, typename OutputIterator, typename Predicate>
inline auto copy_until_if(InputIterator begin, InputIterator end, OutputIterator dest, const Predicate predicate) -> InputIterator
{
  for (; begin != end && !predicate(*begin); ++begin, ++dest) {
    *dest = *begin;
  }

  return begin;
}

template<typename InputIterator, typename Type = decltype(*std::declval<InputIterator>())>
inline auto count_until(InputIterator begin, InputIterator end, const Type &value) -> std::size_t
{
  auto sz{0ULL};

  for (; begin != end && *begin != value; ++begin, ++sz) {
  }

  return sz;
}

template<typename InputIterator, typename Predicate>
inline auto count_until_if(InputIterator begin, InputIterator end, const Predicate predicate) -> std::size_t
{
  auto sz{0ULL};

  for (; begin != end && !predicate(*begin); ++begin, ++sz) {
  }

  return sz;
}

template<typename InputIterator, typename OutputIterator, typename Converter, typename Type = decltype(*std::declval<InputIterator>())>
inline auto transform_until(InputIterator begin, InputIterator end, OutputIterator dest, const Type &value, const Converter converter) -> InputIterator
{
  for (; begin != end && *begin != value; ++begin, ++dest) {
    *dest = converter(*begin);
  }

  return begin;
}

template<typename InputIterator, typename OutputIterator, typename Converter, typename Predicate>
inline auto transform_until_if(InputIterator begin, InputIterator end, OutputIterator dest, const Converter converter, const Predicate predicate)
    -> InputIterator
{
  for (; begin != end && !predicate(*begin); ++begin, ++dest) {
    *dest = converter(*begin);
  }

  return begin;
}

template<typename InputIterator, typename OutputIterator, typename Runner, typename Type = decltype(*std::declval<InputIterator>())>
inline auto for_each_until(InputIterator begin, InputIterator end, const Runner runner, const Type &value) -> InputIterator
{
  for (; begin != end && *begin != value; ++begin) {
    runner(*begin);
  }

  return begin;
}

template<typename InputIterator, typename Runner, typename Predicate>
inline auto for_each_until_if(InputIterator begin, InputIterator end, const Runner runner, const Predicate predicate) -> InputIterator
{
  for (; begin != end && !predicate(*begin); ++begin) {
    runner(*begin);
  }

  return begin;
}

} // namespace aoc

#endif // AOC_HPP
